#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""
# Paquetes
import socket
import sys

# Constantes
ERROR = "Usage: python3 client.py method receiver@IP:SIPport"

# Control de argumentos
try:
    METHOD = str.upper(sys.argv[1])
    RECEIVER = sys.argv[2].split('@')[0]
    IP = sys.argv[2].split('@')[1].split(':')[0]
    SIP_PORT = int(sys.argv[2].split(':')[1])
except:
    sys.exit(ERROR)

# Contenido que vamos a enviar
MENSAJE = " sip:" + RECEIVER + "@" + IP + ":" + str(SIP_PORT)
LINE = METHOD + MENSAJE + " SIP/2.0\r\n"

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, SIP_PORT))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')

    try:
        data = my_socket.recv(1024)
        respuesta = data.decode('utf-8').split('\r\n\r\n')
    except ConnectionRefusedError:
        sys.exit("Conexion fallida")

    if METHOD == "INVITE":
        Condiciones = [respuesta[0].split()[-1] == "Trying",
                       respuesta[1].split()[-1] == "Ring",
                       respuesta[2].split()[-1] == "OK"]

        if all(Condiciones):
            print(respuesta[0] + "\r\n\r\n" +
                  respuesta[1] + "\r\n\r\n" +
                  respuesta[2] + "\r\n\r\n")

            my_socket.send(bytes(LINE.replace("INVITE", "ACK"),
                           "utf-8") + b"\r\n")

        if LINE.split()[0] == "INVITE":
            my_socket.send(bytes(LINE.replace("INVITE", "BYE"),
                           "utf-8") + b"\r\n")
    else:
        print(respuesta[0])

    print("Terminando socket...")
