#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
# Paquetes
import socketserver
import sys
import os

# Constantes
ERROR = 'Usage: python3 server.py IP port audio_file'
METODOS_DEFINIDOS = 'INVITE', 'ACK', 'BYE'

# Control de argumentos
try:
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    if os.path.exists(sys.argv[3]):
        fichero_audio = sys.argv[3]
    else:
        sys.exit('ERROR: Audio no encontrado.')
except:
    sys.exit(ERROR)

# Códigos de respuestas
TRYING = b"SIP/2.0 100 Trying\r\n\r\n"
RINGING = b"SIP/2.0 180 Ring\r\n\r\n"
OK = b"SIP/2.0 200 OK\r\n\r\n"
BAD_REQUEST = b"SIP/2.0 400 Bad Request\r\n\r\n"
METHOD_NOT_ALLOWED = b"SIP/2.0 405 Method Not Allowed\r\n\r\n"

# Envío RTP
aEjecutar = "./mp32rtp -i 127.0.0.1 -p 23032 < " + fichero_audio


# Clase manejador
class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribimos dirección y puerto del cliente
        print("La IP y el puerto del cliente es: " + str(self.client_address))

        # Obtenemos la información
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            MENSAJE = self.rfile.read()
            # Si no hay más líneas salimos del bucle infinito
            if not MENSAJE:
                break

            LINE = MENSAJE.decode("utf-8").split()
            print("El cliente nos manda " + str(LINE) + "\r\n")

            # Definimos la respuesta.
            if len(LINE) == 3 and LINE[2] == "SIP/2.0":
                METODO = LINE[0]
                if METODO in METODOS_DEFINIDOS:
                    if METODO == 'INVITE':
                        self.wfile.write(TRYING + RINGING + OK)
                    elif METODO == 'ACK':
                        print("Vamos a ejecutar", aEjecutar, "\r\n")
                        os.system(aEjecutar)
                    elif METODO == 'BYE':
                        self.wfile.write(OK)
                else:
                    self.wfile.write(METHOD_NOT_ALLOWED)
            else:
                self.wfile.write(BAD_REQUEST)

if __name__ == "__main__":
    # Creamos servidor de RTP
    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Listening..." + "\r\n")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Comunicacion finalizada")
